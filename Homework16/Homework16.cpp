﻿#include <iostream>
#include <ctime>

int main()
{
	const int size = 12;
	int array[size][size];
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			array[i][j] = i + j;
			std::cout << array[i][j] << " ";
		}
		std::cout << std::endl;
	}
	int sum = 0;
	time_t now = time(NULL);
	tm LocalDate;
	localtime_s(&LocalDate, &now);
	int month = (LocalDate.tm_mon) + 1;
	int index = static_cast<int>(month % size);
	for (int j = 0; index < size and j < size; ++j) {
		sum += array[index][j];
	}
	std::cout << sum <<std::endl;

}